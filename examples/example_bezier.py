from Bezier import Bezier
import numpy as np
from numpy import array as a
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#~~~#

fig = plt.figure(dpi=128)

#~~~# Simple arch.
h= 1.5  #height of AlphaVortex
alpha=0.785398  #radian
t_points = np.arange(0, 1, 0.01)
test = a([[0, 0,1], [3, 0.05,0.9],  [3, 0.75,0.5], [ 2+h*np.sin(alpha),h,0.45],[2+h*np.sin(alpha), h,0]])
test_set_1 = Bezier.Curve(t_points, test)

plt.xticks([i1 for i1 in range(-20, 20)]), plt.yticks([i1 for i1 in range(-20, 20)])
plt.gca().set_aspect('equal', adjustable='box')
plt.grid(b=True, which='major', axis='both')

ax = fig.add_subplot(111, projection='3d')
ax.plot(test_set_1[:, 0], test_set_1[:, 1], test_set_1[:, 2])
ax.plot(test[:, 0], test[:, 1], test[:, 2], 'o:')


#plt.plot(test_set_1[:, 0], test_set_1[:, 1])
#plt.plot(test[:, 0], test[:, 1], 'ro:')

plt.show()
