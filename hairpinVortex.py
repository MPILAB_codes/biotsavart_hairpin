__author__ = 'wack'

# part of the magwire package

# calculate Biot Savart induction of a vortex line forming a hairpin vortex

# written by Michael Wack 2015
# wack@geophysik.uni-muenchen.de

# Extended for hairpin vortices by JPH (Aug 2021)

# tested with python 3.4.3

import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
import wire
import biotsavart
import pdb
from Bezier import Bezier


w  = wire.Wire(path=wire.Wire.AlphaVortex(h=0.5,gamma=1.5,lamb=0.5), discretization_length=0.02, current=100).Rotate(axis=(1, 0, 0), deg=90) #.Translate((0.1, 0.1, 0)).
w2 = wire.Wire(path=wire.Wire.AlphaVortex(h=0.5,gamma=1.5,lamb=0.5,translate=-1.5,scale=0.9), discretization_length=0.02, current=100).Rotate(axis=(1, 0, 0), deg=90) #.Translate((0.1, 0.1, 0)).
w3 = wire.Wire(path=wire.Wire.AlphaVortex(h=0.5,gamma=1.5,lamb=0.5,translate=-3,scale=0.8), discretization_length=0.02, current=100).Rotate(axis=(1, 0, 0), deg=90) #.Translate((0.1, 0.1, 0)).

sol  = biotsavart.BiotSavart(wire=w)
sol2 = biotsavart.BiotSavart(wire=w2)
sol3 = biotsavart.BiotSavart(wire=w3)

resolution = 0.05
volume_corner1 = (-2, -1, -1)
volume_corner2 = (3.5, 1, 1)

# matplotlib plot 2D
# create list of xy coordinates
grid = np.mgrid[volume_corner1[0]:volume_corner2[0]:resolution, volume_corner1[1]:volume_corner2[1]:resolution]

# create list of grid points
points = np.vstack(map(np.ravel, grid)).T
points = np.hstack([points, np.zeros([len(points),1])])

# calculate B field at given points
B  = sol.CalculateB(points=points)
B2 = sol2.CalculateB(points=points)
B3 = sol3.CalculateB(points=points)

Babs  = np.linalg.norm(B, axis=1)
Babs2 = np.linalg.norm(B2, axis=1)
Babs3 = np.linalg.norm(B3, axis=1)

# remove big values close to the wire
cutoff = 0.0003
B[Babs > cutoff]   = [np.nan,np.nan,np.nan]
B2[Babs2 > cutoff] = [np.nan,np.nan,np.nan]
B3[Babs3 > cutoff] = [np.nan,np.nan,np.nan]


Ux=B[:, 0]+B2[:,0]+B3[:,0]
Uy=B[:, 1]+B2[:,1]+B3[:,1]
Uz=B[:, 2]+B2[:,2]+B3[:,2]

fig = plt.figure()
ax = fig.gca()
ax.quiver(points[:, 0], points[:, 1], Ux, Uy)
X = np.unique(points[:, 0])
Y = np.unique(points[:, 1])
plt.show()

#cs = ax.contour(points[:, 0], points[:, 1], Ux, 10)
#ax.clabel(cs)
#plt.xlabel('x')
#plt.ylabel('y')
#plt.axis('equal')
#plt.figure()


_, bins_x, _ =plt.hist(Ux,450,alpha=0.4,color='red')
#_, bins_y, _ =plt.hist(Uy,450,alpha=0.4,color='blue')
#_, bins_z, _ =plt.hist(Uz,450,alpha=0.4,color='green')
pdb.set_trace()
mu, sigma = scipy.stats.norm.fit(Ux[~np.isnan(Ux)])
best_fit_line = scipy.stats.norm.pdf(bins_x, mu, sigma)
plt.plot(bins_x, best_fit_line)

plt.xlim([-0.0001,0.0001])
plt.show()

# matplotlib plot 3D
grid = np.mgrid[volume_corner1[0]:volume_corner2[0]:resolution*2, volume_corner1[1]:volume_corner2[1]:resolution*2, volume_corner1[2]:volume_corner2[2]:resolution*2]

# create list of grid points
points = np.vstack(map(np.ravel, grid)).T

# calculate B field at given points
B = sol.CalculateB(points=points)

Babs = np.linalg.norm(B, axis=1)

fig = plt.figure()
# 3d quiver
ax = fig.gca(projection='3d')
sol.mpl3d_PlotWires(ax)
sol2.mpl3d_PlotWires(ax)
sol3.mpl3d_PlotWires(ax)
pdb.set_trace()
#ax.quiver(points[:, 0], points[:, 1], points[:, 2], B[:, 0], B[:, 1], B[:, 2], length=0.09)
plt.show()
